var login = {};

var loadApp = function(){

	if(detectIE()){
		document.body.innerHTML="";
		document.body.style.backgroundImage = "url('microsoft-internet-explorer-sucks.jpg')";
	}
	$("#logoutLink").hide();
	if (JSON.parse(localStorage.getItem("User"))) {
		userLoggedIn(JSON.parse(localStorage.getItem("User")));
	}
	$("#locations").easyAutocomplete(options);
}

var validateEmail = function(emailField, errTxtID){
	 var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;

        if (reg.test(emailField.value) == false) 
        {
			emailField.classList.remove("successField");
			emailField.className = "errorField " + emailField.className;
			document.getElementById(errTxtID).style.display="block";
			return false;
			
        } else {
			emailField.classList.remove("errorField");
			emailField.className = "successField " + emailField.className;
			document.getElementById(errTxtID).style.display="none";
			return true;
		}
	
};

validatePassword = function(passwordField, errTxtID){
	if(passwordField.value && passwordField.value.length > 7) {
		passwordField.classList.remove("errorField");
		passwordField.className = "successField " + passwordField.className;
		document.getElementById(errTxtID).style.display='none';
		return true;
	} else {
		passwordField.classList.remove("successField");
		passwordField.className = "errorField " + passwordField.className;
		document.getElementById(errTxtID).style.display="block";
		return false;
	}

};


var validateName = function(nameField, errTxtID){
	if(nameField.value.length > 1){
		nameField.classList.remove("errorField");
		nameField.className = "successField " + nameField.className;
		document.getElementById(errTxtID).style.display="none";
		return true;
	} else {
		nameField.classList.remove("successField");
		nameField.className = "errorField " + nameField.className;
		document.getElementById(errTxtID).style.display="block";
		return false;
	}

};

var validateREPassword = function(passwordField, errTxtID){
	if(passwordField.value && passwordField.value.length > 7 && passwordField.value === document.getElementById('signupPassword').value) {
		passwordField.classList.remove("errorField");
		passwordField.className = "successField " + passwordField.className;
		document.getElementById(errTxtID).style.display="none";
		return true;
	} else {
		passwordField.classList.remove("successField");
		passwordField.className = "errorField " + passwordField.className;
		document.getElementById(errTxtID).style.display="block";
		return false;
	}

};


var loginForm = function(){
		document.getElementById('LoadSpinner').style.display="block";
		$.ajax({
			url: '/StayBIDD/LoginAction',
			data:{userName: $("#loginEmailId").val(),password: $("#loginPassword").val()},
			dataType: 'json',
			success: loginFormResult,
			error: function(data){console.log(data);}
			});
};

var loginFormResult = function(data) {
	myVar = setTimeout(function(){
		document.getElementById('LoadSpinner').style.display="none";
		document.getElementById('notifier').style.display='block';
		if(data.error){
			document.getElementById('notifier').innerHTML=data.error;
			document.getElementById('notifier').style.background = "#F92400";
			window.location.hash = '';
		} else {
			document.getElementById('notifier').innerHTML=data.User.name + " logged in successFully !";
			document.getElementById('notifier').style.background = "#6DE601";
			userLoggedIn(data.User);
		}
	}, 500);
	console.log(data);
};

var userLoggedIn = function(User) {
	window.location.hash = User.name;
	$(".commonContainer").hide();
	$(".userContainer").show();
	$("#loginLink").hide();
	$("#logoutLink").show();
	$(document).prop('title', User.name);
	localStorage.setItem("User", JSON.stringify(User));
	displayUserInfo(User);
};

var displayUserInfo = function(user) {
	var data= "<li><img src='css/user.png' id='userImage'></li>";
	for(var key in user) {
		if(key && user[key]){
			data+="<li>"+user[key]+"</li>";
		}
	}
	data+="<li><a href='#'><img src='css/rsz_edit.png'></a></li>";
	$("#userInfo").html(data);
};

var logout = function(){
	$(".commonContainer").show();
	$(".userContainer").hide();
	$("#loginLink").show();
	$("#logoutLink").hide();
	localStorage.setItem("User", null);
};
var toggleProfile = function() {
	// $( "#leftPanel" ).toggle( "slide", {}, 10000 );
	$("#leftPanel").animate({"left": '+=200px'}, 'slow');

};