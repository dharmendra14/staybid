  $(function() {
    $( "#from" ).datepicker({
      defaultDate: "+1w",
      changeMonth: true,
      numberOfMonths: 1,
      dateFormat: "DD, d MM, yy",
      minDate: 0,
      maxDate: "+1M +10D",
      onClose: function( selectedDate ) {
        $( "#to" ).datepicker( "option", "minDate", selectedDate );
      }
    }).datepicker("setDate", "0");;
    $( "#to" ).datepicker({
      defaultDate: "+1w",
      changeMonth: true,
      numberOfMonths: 1,
      dateFormat: "DD, d MM, yy",
      maxDate: "+1M +10D",
      onClose: function( selectedDate ) {
        $( "#from" ).datepicker( "option", "maxDate", selectedDate );
        var days   = ($('#to').datepicker('getDate') - $('#from').datepicker('getDate'))/1000/60/60/24;
        $("#nbDays").val(days);
      }
    }).datepicker("setDate", "1");
  });