/**
 *  JS file for search
 */

var options = {
	data: ["Bangalore", "Pune", "Goa", "Delhi", "Chennai", "Kolkata","Jhansi","Shiradi","Bekal","Pondicherry","Mumbai","Hydrabad"],
	placeholder: "Type your visiting place"

};

var totalGuest = 1;
var searchData = "";
function decsGuest() {
	if(totalGuest > 1) {
		$("#guests").val(--totalGuest);
	} else {
		alert("Minimum no of guest is 1");
	}
	return false;
}

function incsGuest() {
	$("#guests").val(++totalGuest);
	return false;
}

function serachResult() {
	searchData = $('#searchForm').serializeArray();
	document.getElementById('LoadSpinner').style.display="block";
	alert(JSON.stringify(searchData));
}
