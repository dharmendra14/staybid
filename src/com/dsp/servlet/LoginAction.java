package com.dsp.servlet;

import java.io.IOException;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.dsp.helper.UserHelper;
import com.dsp.user.User;
import com.google.gson.Gson;

/**
 * Servlet implementation class loginAction
 */
public class LoginAction extends HttpServlet {
	private static final long serialVersionUID = 1L;
    /**
     * @see HttpServlet#HttpServlet()
     */
    public LoginAction() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String json = null;
		User user = null;
		HashMap<Object, Object> result = new HashMap<Object, Object>();
		System.out.println(request.getParameter("userName") +" "+ request.getParameter("password"));
		user = UserHelper.checkUser(request.getParameter("userName"), request.getParameter("password"));
		if(user != null){
			result.put("User", user);
		} else {
			result.put("error", "Opps login Failed, Try again !!");
		}
		json = new Gson().toJson(result);
        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        response.getWriter().write(json);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
