package com.dsp.helper;

import com.dsp.user.User;

public class UserHelper {
	
	public static User createUser() {
		User user = new User();
		user.setName("Dharmendra");
		user.setEmail("Dharmendra.patel26@gmail.com");
		user.setAge(24);
		user.setContact("7760924707");
		user.setAddress("Ayyappa layout 2nd cross munnekolalla marathahalli bangalore karnataka 560037");
		return user;
	}
	
	public static User checkUser(String username, String Password) {
		User user = createUser();
		if(user.getEmail().equalsIgnoreCase(username) && Password.equalsIgnoreCase("1234567890")) return user;
		return null;
	}
}
