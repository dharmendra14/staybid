package com.dsp.user;

public class User {
	int id ;
	String name;
	String email;
	int age;
	String address;
	String contact;
	
	public User(int id, String name, String email, int age, String address, String contact) {
		this.id = id;
		this.name = name;
		this.email = email;
		this.age = age;
		this.address = address;
		this.contact = contact;
	}

	public User() {
		// TODO Auto-generated constructor stub
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getContact() {
		return contact;
	}

	public void setContact(String contact) {
		this.contact = contact;
	}
	
}
